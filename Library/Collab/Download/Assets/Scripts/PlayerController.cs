﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour
{
    public float speed = 0f;
    public float tilt;
    public Boundary boundary;
    private Rigidbody rgdBody;

    private void Start()
    {
        rgdBody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rgdBody.velocity = movement * speed;

        rgdBody.position = new Vector3
            (
                Mathf.Clamp(rgdBody.position.x, boundary.xMin, boundary.xMax),
                0.0f,
                Mathf.Clamp(rgdBody.position.z, boundary.zMin, boundary.zMax)
            );

        rgdBody.rotation = Quaternion.Euler(0.0f, 0.0f, rgdBody.velocity.x * -tilt);
    }
}