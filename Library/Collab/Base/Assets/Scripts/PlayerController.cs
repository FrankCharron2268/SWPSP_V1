﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour
{
    public float speed = 0f;
    public Boundary boundary;
    private Rigidbody rigbody;

    private void Start()
    {
        rigbody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rigbody.velocity = movement * speed;

        rigbody.position = new Vector3
            (
            Mathf.Clamp(rigbody.position.x, boundary.xMin, boundary.xMax),
            0.0f,
            Mathf.Clamp(rigbody.position.z, boundary.zMin, boundary.zMax)
            );
    }
}