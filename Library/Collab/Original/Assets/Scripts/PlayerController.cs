﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour
{
    private Rigidbody rgdBody;
    private float nextFire;

    public Boundary boundary;
    public GameObject projectile;
    public Transform[] firePoss;
    public float speed = 0f;
    public float tilt;
    public float fireRate;

    private void Start()
    {
        rgdBody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        //Instantiate(projectile, firePoss.position, firePoss.rotation);
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            //StartCoroutine("CyclingFireMode");
            QuadFireMode();
        }
    }

    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rgdBody.velocity = movement * speed;

        rgdBody.position = new Vector3
            (
                Mathf.Clamp(rgdBody.position.x, boundary.xMin, boundary.xMax),
                0.0f,
                Mathf.Clamp(rgdBody.position.z, boundary.zMin, boundary.zMax)
            );

        rgdBody.rotation = Quaternion.Euler(0.0f, 0.0f, rgdBody.velocity.x * -tilt);
    }

    public void QuadFireMode()
    {
        for (int i = 0; i < firePoss.Length; i++)
        {
            Instantiate(projectile, firePoss[i].position, firePoss[i].rotation);
        }
    }

    public IEnumerator OffsetFireMode()
    {
        //TODO Optimize
        fireRate = .5f;
        Instantiate(projectile, firePoss[0].position, firePoss[0].rotation);
        yield return new WaitForSeconds(0.02f);
        Instantiate(projectile, firePoss[2].position, firePoss[2].rotation);
        yield return new WaitForSeconds(0.8f);
        Instantiate(projectile, firePoss[1].position, firePoss[1].rotation);
        yield return new WaitForSeconds(0.02f);
        Instantiate(projectile, firePoss[3].position, firePoss[3].rotation);
    }

    public IEnumerator CyclingFireMode()
    {
        for (int i = 0; i < firePoss.Length; i++)
        {
            fireRate = 1f;
            Instantiate(projectile, firePoss[i].position, firePoss[i].rotation);
            yield return new WaitForSeconds(0.25f);
        }
    }
}