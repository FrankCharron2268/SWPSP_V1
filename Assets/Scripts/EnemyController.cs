﻿using System.Collections;
using UnityEngine;

public class EnemyController : BaseController
{
    [Header("Misc Settings")]
    public float fireDelay;                               //Time left before next shot.
    [Header("Movement Settings")]
    public Boundary boundary;                           //Values that limit's player movement inside the screen.
    public float tilt;                                 //Ship tilt movement value.
    public float dodge;                               //The bigger the value the bigger the sway.
    public float smoothingManeuver;                  //This value smoothes the sway over time
    public Vector2 startWait;                       //Wait before maneuvering
    public Vector2 maneuverTime;                   //Time to execute maneuver
    public Vector2 maneuverWait;                  //Time between maneuvers

    private float currentSpeed;
    private float targetManeuver;

    private void Update()
    {
        NextFire = Time.time + fireRate;
    }

    private void Start()
    {
        Move();
        Destroy(gameObject, lifeTime);

        Audiosource = GetComponent<AudioSource>();
        InvokeRepeating("FireShot", fireDelay, Random.Range(fireRate, fireRate + 1));
        currentSpeed = RgdBody.velocity.z;
        StartCoroutine("Evade");

        GameObject gameControllerGO = GameObject.FindWithTag("GameController");
        gameController = gameControllerGO.GetComponent<GameController>();
    }

    private void FixedUpdate()
    {
        float newManeuver = Mathf.MoveTowards(RgdBody.velocity.x, targetManeuver, Time.deltaTime * smoothingManeuver);
        RgdBody.velocity = new Vector3(newManeuver, 0.0f, currentSpeed);
        RgdBody.position = new Vector3(
          Mathf.Clamp(RgdBody.position.x, boundary.xMin, boundary.xMax),
          0.0f,
          Mathf.Clamp(RgdBody.position.z, boundary.zMin, boundary.zMax)
        );
        RgdBody.rotation = Quaternion.Euler(0.0f, 180, RgdBody.velocity.x * -tilt);
    }

    private void FireShot()
    {
        Instantiate(projectile, fireTransforms[0].position, fireTransforms[0].rotation);
        Instantiate(projectile, fireTransforms[1].position, fireTransforms[1].rotation);
        Audiosource.Play();
    }

    private void Move()
    {
        RgdBody = GetComponent<Rigidbody>();
        RgdBody.velocity = transform.forward * speed;
    }

    private IEnumerator Evade()
    {
        yield return new WaitForSeconds(Random.Range(startWait.x, startWait.y));

        while (true)
        {//TODO currently enemies will always maneuver inward change this so they can maneuver outward too while still staying inscreen. OR mayb exit screen but comeback.
            targetManeuver = Random.Range(1, dodge) * -Mathf.Sign(transform.position.x);//Change Here
            yield return new WaitForSeconds(Random.Range(maneuverTime.x, maneuverTime.y));
            targetManeuver = 0;
            yield return new WaitForSeconds(Random.Range(maneuverTime.x, maneuverTime.y));
        }
    }
}