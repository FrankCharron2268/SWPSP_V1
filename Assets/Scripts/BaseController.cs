﻿using UnityEngine;
using UnityEngine.UI;

//Base Class for all object's controller.
public class BaseController : MonoBehaviour
{
    protected AudioSource Audiosource { get; set; }       //Reference to the GO's audiosource component.
    public bool isPlayer;                                //Used to determine if object is the player or not.

    protected GameController gameController { get; set; }   //Reference to the GameController Component in the scene. Note: I dont care about convention regulation for this one. It would be confusing to use Capital on this one.

    protected Rigidbody RgdBody { get; set; }             //Reference to the Go's rigidbody component.
    public bool destroyByContact = false;
    public GameObject Explosion;                        //Go's Explosion FX.
    public float pointValue;                           //Score value earned by played when this object is destroyed.

    [Header("Health Settings")]
    public float maxHP;                             //Object's Hit Points.
    protected float CurrentHP { get; set; }        //Player remaining HP.
    public float maxShield;                       //Object's Shield Points.

    protected float CurrentShield { get; set; } //Player remaining Shields.
    public Slider healthBar;                   //Reference to the health bar children object.
    public Slider shieldBar;                  //Reference to the shield bar children object.
    public float lifeTime;                   //Non player lifetime before they are automatically destroyed.

    [Header("Weapon Settings")]
    public GameObject projectile;         //Reference used to instantiate projectiles when firing.

    public Transform[] fireTransforms;  //References to possible fire spawn points(canon ends).

    public int Torpedoes;             //Holds number of torpedoes and can be set for starting number in inspector.
    public float fireRate;

    protected float NextFire { get; set; }  //Time before can fire again/

    [Header("Movement Settings")]
    public float speed;      //Speed at which player or object moves in game space.
    public float tumble;    //Rate at which object(asteroids) roll/tumble while moving.

    public virtual void CalculateDamage(float damage)
    {
        if (CurrentShield <= 0)//If shields are down
        {
            CurrentHP -= damage;//Take HP damage
            if (CurrentHP <= 0)
            {
                Die();
            }
        }
        else //If shields are still up.
        {
            CurrentShield -= damage;//Take shield damage.
        }
        if (isPlayer)
        {
            healthBar.value = CalculateHealth(); //Sets the HP bar to HP Value
            shieldBar.value = CalculateShield();//Sets the Shield bar to Shield Value
        }
    }

    public virtual float CalculateHealth()
    {
        return CurrentHP / maxHP;   //Returns hp on a scale of 1 since our health and shield bar has a value of 0 to 1.
    }

    public virtual float CalculateShield()  //Returns shields on a scale of 1 since our shield and health bar has a value of 0 to 1.
    {
        return CurrentShield / maxShield;
    }

    public void AddScore()
    {
        gameController.AddPoints(pointValue);   //Called when game object is destroyed. Add its value to the game controller score value.
    }

    public virtual void SimpleMove()
    {
        RgdBody.velocity = transform.forward * speed;
    }   //Sets objects moving forward(downscreen-ward).

    public virtual void Tumble()        //Sets a random tumble direction to objects such as asteroids and powerups.
    {
        RgdBody.angularVelocity = Random.insideUnitSphere * tumble;
    }       //

    public virtual void Die() //Called when Object HP reaches 0
    {
        Destroy(gameObject);
        Instantiate(Explosion, transform.position, transform.rotation); //Spawn Explosion FX that has been selected in the gameobject's inspector.
        AddScore();
    }
}