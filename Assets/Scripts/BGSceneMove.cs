﻿using UnityEngine;

public class BGSceneMove : MonoBehaviour
{
    public float speed;
    private Rigidbody rigdbody;

    private void Start()
    {
        rigdbody = GetComponent<Rigidbody>();
        rigdbody.velocity = transform.forward * speed;
    }
}