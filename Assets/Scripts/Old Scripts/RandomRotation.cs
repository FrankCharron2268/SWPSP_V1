﻿using UnityEngine;

public class RandomRotation : MonoBehaviour
{
    private Rigidbody rigdbody;

    public float tumble;

    private void Start()
    {
        rigdbody = GetComponent<Rigidbody>();
        rigdbody.angularVelocity = Random.insideUnitSphere * tumble;
    }
}