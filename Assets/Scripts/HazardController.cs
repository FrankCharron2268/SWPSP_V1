﻿using UnityEngine;

public class HazardController : BaseController
{
    private void Start()
    {
        Destroy(gameObject, lifeTime);
        RgdBody = GetComponent<Rigidbody>();
        SimpleMove();
        Tumble();

        GameObject gameControllerGO = GameObject.FindWithTag("GameController");
        gameController = gameControllerGO.GetComponent<GameController>();
    }
}