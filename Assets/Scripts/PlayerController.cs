﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;        //Defines maximum game space for player.
}

public class PlayerController : BaseController
{
    [HideInInspector]
    public bool
        OffSetFire,                         //Offset is always true as it is the starting weapon, the 2 others will be set to true as player picks up power ups.
        QuadFire = false,
        CyclingFire = false;
    private int selectedWeapon = 1;       //Players current weapon selection.

    [Header("Movement Settings")]
    public Boundary boundary;           //Values that limit's player movement inside the screen.
    public float tilt;                 //Ship tilt movement value.

    private void Start()
    {
        CurrentHP = maxHP;                             //------------------------------
        CurrentShield = maxShield;                    //Sets the player's HP And shields.
        healthBar.value = CalculateHealth();         //----------------------------------
        shieldBar.value = CalculateShield();        //Sets the player's Hp and Shield Bars.

        RgdBody = GetComponent<Rigidbody>();       //-------------------------------------------------------
        Audiosource = GetComponent<AudioSource>();//Gets the reference to the Go's Rigidbody and Audiosource

        GameObject gameControllerGO = GameObject.FindWithTag("GameController");     //Finds GO in scene tagged GameController.
        gameController = gameControllerGO.GetComponent<GameController>();          //Finds the GameController component(script) on our GameController Game object. And save it for references.
    }

    private void Update()
    {
        CheckWeaponSelection();//Check which weapon is currently selected.

        if (Input.GetButton("Fire1") && Time.time > NextFire)   //If Fire1 button is pressed and delay as passed.
        {
            NextFire = Time.time + fireRate;                   //Sets the time at which we can fire next.

            switch (selectedWeapon)     //Fire selected weapon mode.
            {
                case 1:
                    StartCoroutine("OffsetFireMode");
                    break;

                case 2:
                    QuadFireMode();
                    break;

                case 3:
                    StartCoroutine("CyclingFireMode");
                    break;

                case 4:
                    //TODO add
                    break;
            }
        }
    }

    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal"); //detect input from horizontal key.
        float moveVertical = Input.GetAxis("Vertical");//detect input from vertical key.

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);     //Creates our movement vector.
        RgdBody.velocity = movement * speed; //sets the velocity of our objets rigidbody.

        RgdBody.position = new Vector3
            (
                Mathf.Clamp(RgdBody.position.x, boundary.xMin, boundary.xMax),  //Sets the X maximum player movement.
                0.0f,
                Mathf.Clamp(RgdBody.position.z, boundary.zMin, boundary.zMax)   //Sets the Z maximum player movement(see it as being Y).
            );

        RgdBody.rotation = Quaternion.Euler(0.0f, 0.0f, RgdBody.velocity.x * -tilt);    //While moving sideway our ship will tilt on the sides.Tilt value can be set in inspector.
    }

    public void QuadFireMode()
    {
        fireRate = .5f;
        for (int i = 0; i < fireTransforms.Length; i++)
        {
            Instantiate(projectile, fireTransforms[i].position, fireTransforms[i].rotation);
        }
        Audiosource.Play();
    }//Fires all canons at once.

    public IEnumerator OffsetFireMode() //Fires canons 1,3 and then 2,4.
    {    //Wrote this function hastily as temporary solution
         //TODO Optimize
        fireRate = .5f;
        Instantiate(projectile, fireTransforms[0].position, fireTransforms[0].rotation);
        yield return new WaitForSeconds(0.01f);
        Instantiate(projectile, fireTransforms[2].position, fireTransforms[2].rotation);
        Audiosource.Play();
        yield return new WaitForSeconds(0.1f);
        Instantiate(projectile, fireTransforms[1].position, fireTransforms[1].rotation);
        yield return new WaitForSeconds(0.01f);
        Instantiate(projectile, fireTransforms[3].position, fireTransforms[3].rotation);
        Audiosource.Play();
    }

    public IEnumerator CyclingFireMode()    //Cycles through the 4 canons and fires rapidly.
    {
        fireRate = 0.7f;
        for (int i = 0; i < fireTransforms.Length; i++)
        {
            Instantiate(projectile, fireTransforms[i].position, fireTransforms[i].rotation);
            Audiosource.Play();
            yield return new WaitForSeconds(0.167f);
        }
    }

    public void CheckWeaponSelection()
    {
        if (Input.GetButton("Weapon1"))
        {
            selectedWeapon = 1;
            Debug.Log("Offset laser Selected");
        }
        else if (Input.GetButton("Weapon2") && QuadFire)
        {
            selectedWeapon = 2;
            Debug.Log("Quad Laser Selected");
        }
        else if (Input.GetButton("Weapon3") && CyclingFire)
        {
            selectedWeapon = 3;
            Debug.Log("Cycling laser Selected");
        }
        else if (Input.GetButton("Weapon4") && Torpedoes > 0)
        {
            selectedWeapon = 4;
            Debug.Log("Torpedoes Selected");
        }
    }
}