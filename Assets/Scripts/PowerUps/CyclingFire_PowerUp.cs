﻿using UnityEngine;

public class CyclingFire_PowerUp : BasePowerUp
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PowerUp();

            AttributePoints();
            DestroyPowerUp();
        }
    }

    public override void PowerUp()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        playerController.CyclingFire = true;
    }
}