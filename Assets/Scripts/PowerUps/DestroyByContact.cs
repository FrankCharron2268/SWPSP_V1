﻿using UnityEngine;

public class DestroyByContact : MonoBehaviour
{
    public float pointsValue;
    public GameObject explosion;

    //public GameObject playerExplosion;
    private GameController gameController;

    private void Start()
    {
        GameObject gameControllerGO = GameObject.FindWithTag("GameController");
        if (gameControllerGO != null)
        {
            gameController = gameControllerGO.GetComponent<GameController>();
            if (gameControllerGO == null)
            {
                Debug.Log("Cannot find 'GameController' script");
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //TODO THIS IS WRONG. MY PROJECTILES ARE TAGGED RESPECTIVELY PLAYER, ENEMY AND THEY SHOULDNT THATS WHY THE CONDITION IS SO LONG. FIX IT //Fixed i think
        if (other.CompareTag("Boundary") || gameObject.CompareTag("Enemy") && other.CompareTag("Enemy") || gameObject.CompareTag("Player") && other.CompareTag("Player") || other.CompareTag("PowerUp"))
        {
            return;
        }

        Instantiate(explosion, transform.position, transform.rotation);

        if (other.tag == "Player")
        {
            //Instantiate(explosion, other.transform.position, other.transform.rotation);
            //gameController.GameOver()
        }
        gameController.AddPoints(pointsValue);
        Destroy(other.gameObject);
        Destroy(gameObject);
    }
}