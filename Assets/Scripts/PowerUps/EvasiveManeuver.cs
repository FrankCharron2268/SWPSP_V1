﻿using System.Collections;
using UnityEngine;

public class EvasiveManeuver : MonoBehaviour
{
    public float dodge;
    public float smoothingManeuver;
    public float tilt;
    public Vector2 startWait;
    public Vector2 maneuverTime;
    public Vector2 maneuverWait;
    public Boundary boundary;

    private float currentSpeed;
    private float targetManeuver;
    private Rigidbody rigdBody;

    private void Start()
    {
        rigdBody = GetComponent<Rigidbody>();
        currentSpeed = rigdBody.velocity.z;
        StartCoroutine("Evade");
    }

    private IEnumerator Evade()
    {
        yield return new WaitForSeconds(Random.Range(startWait.x, startWait.y));

        while (true)
        {//TODO currently enemies will always maneuver inward change this so they can maneuver outward too while still staying inscreen. OR mayb exit screen but comeback.
            targetManeuver = Random.Range(1, dodge) * -Mathf.Sign(transform.position.x);//Change Here
            yield return new WaitForSeconds(Random.Range(maneuverTime.x, maneuverTime.y));
            targetManeuver = 0;
            yield return new WaitForSeconds(Random.Range(maneuverTime.x, maneuverTime.y));
        }
    }

    private void FixedUpdate()
    {
        float newManeuver = Mathf.MoveTowards(rigdBody.velocity.x, targetManeuver, Time.deltaTime * smoothingManeuver);
        rigdBody.velocity = new Vector3(newManeuver, 0.0f, currentSpeed);
        rigdBody.position = new Vector3
            (
            Mathf.Clamp(rigdBody.position.x, boundary.xMin, boundary.xMax),
            0.0f,
            Mathf.Clamp(rigdBody.position.z, boundary.zMin, boundary.zMax)
           );
        rigdBody.rotation = Quaternion.Euler(0.0f, 180, rigdBody.velocity.x * -tilt);
    }
}