﻿using System.Reflection.Emit;
using UnityEngine;

public class BasePowerUp : MonoBehaviour
{
    public float pointsValue;

    [HideInInspector]
    public PlayerController playerController;

    public virtual void PowerUp()
    {
    }

    public virtual void AttributePoints()
    {
        //playerController.score += pointsValue;
    }

    public virtual void DestroyPowerUp()
    {
        Destroy(gameObject);
    }
}