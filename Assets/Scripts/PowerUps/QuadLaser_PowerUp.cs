﻿using UnityEngine;

public class QuadLaser_PowerUp : BasePowerUp
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PowerUp();

            AttributePoints();
            DestroyPowerUp();
        }
    }

    public override void PowerUp()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        playerController.QuadFire = true;
    }
}