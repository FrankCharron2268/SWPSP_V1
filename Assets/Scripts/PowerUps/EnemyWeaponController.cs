﻿using UnityEngine;

public class EnemyWeaponController : MonoBehaviour
{
    //TODO USELESS. MOVED TO HAZARDCONTROLLER/
    public GameObject shot;                //References projectile's GO.
    public Transform[] fireTransforms;           //References projectile's spawn Location.
    public float fireRate;               //Sets the fire rate value.
    public float delay;                 //Time left before next shot.

    private AudioSource audioSource;  //References GO audio source.

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        InvokeRepeating("FireShot", delay, Random.Range(fireRate, fireRate + 1));
    }

    private void FireShot()
    {
        Instantiate(shot, fireTransforms[0].position, fireTransforms[0].rotation);
        Instantiate(shot, fireTransforms[1].position, fireTransforms[1].rotation);
        audioSource.Play();
    }
}