﻿using UnityEngine;

public class Torpedo_PowerUp : BasePowerUp
{
    public int torpedoBonus;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PowerUp();

            AttributePoints();
            DestroyPowerUp();
        }
    }

    public override void PowerUp()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        playerController.Torpedoes += torpedoBonus;
    }
}