﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    private float score = 0f;

    [Header("PowerUp Settings")]
    public GameObject[] powerUps;
    public Vector2 powerSpawnRate;        //using a vector2 for range of 2 values to create a random spawn rate between those.

    [Header("Hazard Settings")]
    public GameObject[] hazards;        //List referencing possible hazards
    public Vector3 spawnValues;        //Hazard spawn values making them spawn of screen and within X limits.
    public int hazardCount;           //Number of hazard that spawns per waves.
    public float spawnWait;          //Rate at which hazards spawn.
    public float starWait;          //Time before first wave starts.
    public float waveWait;         //Time between waves.

    [Header("Display Settings")]
    public Text scoreText;

    private void Start()
    {
        StartCoroutine("SpawnWaves");
        StartCoroutine("SpawnPowerUps");
    }

    private IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(starWait);
        while (true)
        {
            for (int i = 0; i < hazardCount; i++)
            {
                GameObject hazard = hazards[Random.Range(0, hazards.Length)];
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;

                Instantiate(hazard, spawnPosition, hazard.transform.rotation);

                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);
        }
    }

    private IEnumerator SpawnPowerUps()
    {
        yield return new WaitForSeconds(starWait);
        while (true)
        {
            for (int i = 0; i < powerUps.Length; i++)
            {
                float wait = Random.Range(powerSpawnRate.x, powerSpawnRate.y);
                yield return new WaitForSeconds(wait);

                GameObject powerUp = powerUps[Random.Range(0, powerUps.Length)];
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                if (powerUp != null)
                {
                    Instantiate(powerUp, spawnPosition, powerUp.transform.rotation);
                }
            }
        }
    }

    public void AddPoints(float Points)
    {
        score += Points;
        scoreText.text = "score " + score;
    }
}