﻿using UnityEngine;

public class BackgroundScroller : MonoBehaviour
{
    public float scrollSpeed;  //Speed at which the backgound repeats
    public float tileSizeZ;

    private Vector3 startPosition;

    private void Start()
    {
        startPosition = transform.position;
    }

    private void Update()
    {
        float newPosition = Mathf.Repeat(Time.time * scrollSpeed, tileSizeZ);
        transform.position = startPosition + Vector3.forward * newPosition;
    }
}