﻿using UnityEngine;

public class SquadUnit : MonoBehaviour
{
    [HideInInspector] public float gSpeed;
    [Range(1f, 20f)] public float speed;

    public float rotationSpeed = 2.0f; //This how fast the ships can turn.
    private Vector3 avgHeading;        //Average heading direction of the squad.
    private Vector3 avgPosition;      //Average position of the squad.
    public float neighbourDistance = 3.0f;     //Maximum distance at which the ship has to be to form up with another. If too far it'll fly alone.

    [HideInInspector] public SquadManager squadManager;
    private bool turning = false;
    public float speedMult = 1;

    private Transform target;
    //private float followSmooth;
    //private Vector3 _followOffset;
    //private bool chasing = false;
    //private const float REFERENCE_FRAMERATE = 30f;

    private void Start()
    {
        //if (target != null)
        //{
        //    _followOffset = this.transform.position - target.transform.position;
        //}
        speed = Random.Range(speed, speed + 2);
        squadManager = GameObject.FindGameObjectWithTag("Squad Manager").GetComponent<SquadManager>();
        // Use this to give a small random speed variation.
    }

    //private void LateUpdate()
    //{
    //    if (chasing)
    //    {
    //        Debug.Log(chasing);
    //        //Apply that offset to get a target position.
    //        Vector3 targetPosition = target.transform.position + _followOffset;
    //        //Keep our y position unchanged.
    //        targetPosition.y = transform.position.y;
    //        //Smooth follow.
    //        float timeRatio = Time.deltaTime * REFERENCE_FRAMERATE;
    //        float adjustedSharpness = 1f - Mathf.Pow(1f - followSmooth, timeRatio);

    //        transform.position += (targetPosition - transform.position) * adjustedSharpness;
    //    }
    //}

    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.CompareTag("Enemy") && !chasing)
    //    {
    //        target = other.transform;
    //        chasing = true;
    //    }
    //}

    private void Update()
    {
        //if (chasing)
        //{
        //    return;
        //}
        //determines the bounding box of the manager cube.
        Bounds b = new Bounds(squadManager.transform.position, squadManager.boundary * 2);
        //if ship is outside the bounds then start turning around.
        if (!b.Contains(transform.position))//If ship reaches limit it'll turn.
        {
            turning = true;
        }
        else { turning = false; }
        if (turning)
        {
            Vector3 direction = squadManager.transform.position - transform.position;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
            speed = speed * speedMult;
        }
        else
        {
            if (Random.Range(0, 5) < 1)//Every 1 in 5 times
            {
                ApplyRules();
            }
        }
        transform.Translate(0, 0, Time.deltaTime * speed * speedMult); // moves the object forward.
    }

    //private Vector3 FollowTarget(GameObject target)
    //{
    //    Vector3 tv = target.GetComponent<Rigidbody>().velocity;
    //    Vector3 force = new Vector3;

    //    tv = tv * -1;
    //    tv.Normalize() ;

    //    return force;
    //}

    private void ApplyRules()
    {
        GameObject[] gos;
        gos = squadManager.allShips;

        Vector3 vCenter = Vector3.zero; //points the center of the group.
        Vector3 vAvoid = Vector3.zero;  //Points away from the other
        gSpeed = 0.01f;            //Group speed.

        Vector3 goalPos = squadManager.goalPos;  //Grabs goal position.

        float dist;

        int groupSize = 0; //Size of flocking group. Counts ships that are in the neighbouring distance.

        foreach (GameObject go in gos)
        {
            if (go != this.gameObject) //if the object is not itself
            {
                dist = Vector3.Distance(go.transform.position, this.transform.position); //Get the distance between itself and the other.

                if (dist <= neighbourDistance)  //If its in the neighbouring distance.

                {
                    vCenter += go.transform.position;   //update group center vector.
                    groupSize++;  //increment group size.
                    if (dist < 1.0f) //If distance is smaller than 1 we are about to collide.
                    {
                        vAvoid = vAvoid + (this.transform.position - go.transform.position); //Calculates avoid vector.
                    }

                    SquadUnit anotherFlock = go.GetComponent<SquadUnit>(); //Grab the script attached to neighboring ship.//change name
                    gSpeed = gSpeed + anotherFlock.speed;   // Get the speed of that neighbor and calculate avg group speed.
                }
            }
        }
        if (groupSize > 0) //if ship is in a group.
        {
            vCenter = vCenter / groupSize + (goalPos - this.transform.position); //Caculate the avg center of the group.
            speed = gSpeed / groupSize * speedMult;  //Caculate the avg speed of the group.

            Vector3 direction = (vCenter + vAvoid) - transform.position; //Calculate group direction.
            if (direction != Vector3.zero)  //If our direction is not 0. Then ship needs to turn.
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
            }
        }
    }
}