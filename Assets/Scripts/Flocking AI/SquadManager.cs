﻿using UnityEngine;

public class SquadManager : MonoBehaviour
{
    [HideInInspector] public SquadManager myFlock;
    public GameObject shipPrefab;

    public int squadSize = 20;
    [HideInInspector] public GameObject[] allShips;
    [HideInInspector] public Vector3 goalPos = Vector3.zero;

    public Vector3 boundary = new Vector3(15, 15, 15);//its actual side length will be twice the values given here.
    public int spaceSize = 30;   //The size of the flocking area

    public void ShipSpeed(float speed)
    {
        Debug.Log(speed);
        for (int i = 0; i < squadSize; i++)
        {
            allShips[i].GetComponent<SquadUnit>().speed = speed;
        }
    }

    private void OnDrawGizmosSelected()     //Draws directional arrows and gizmos for debugging purposes in editor
    {
        Gizmos.color = new Color(1, 0, 0, 0.5F);
        Gizmos.DrawCube(transform.position, new Vector3(boundary.x * 2, boundary.y * 2, boundary.z * 2));
        Gizmos.color = new Color(0, 1, 0, 1);
        Gizmos.DrawSphere(goalPos, 0.1f);
    }

    private void Start()
    {
        allShips = new GameObject[squadSize];    //number of flocking ships

        myFlock = this;
        goalPos = this.transform.position;
        RenderSettings.fogColor = Camera.main.backgroundColor;
        RenderSettings.fogDensity = .03F;
        RenderSettings.fog = true;
        for (int i = 0; i < squadSize; i++)
        {
            Vector3 pos = this.transform.position + new Vector3(Random.Range(-boundary.x, boundary.x),     //Sets each ship to its spawn position
                                      Random.Range(-boundary.y, boundary.y),                              //within the set boundary
                                      Random.Range(-boundary.z, boundary.z));
            try
            {
                allShips[i] = Instantiate(shipPrefab, pos, Quaternion.identity);
                allShips[i].GetComponent<SquadUnit>().squadManager = this;
            }
            catch (MissingComponentException) { }
        }
    }

    private void Update()
    {
        if (Random.Range(0, 10000) < 50) // every 50 in 10000 times it randomly resets the goalPos in the space area.
        {
            goalPos = this.transform.position + new Vector3(Random.Range(-boundary.x, boundary.x),      //Sets the goal position
                                      Random.Range(-boundary.y, boundary.y),                           //Within the limits of the boundary
                                      Random.Range(-boundary.z, boundary.z));                         //set here
        }
    }
}