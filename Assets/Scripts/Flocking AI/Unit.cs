﻿using UnityEngine;

public class Unit : MonoBehaviour
{
    public GameObject manager;
    public Vector2 location = Vector2.zero;
    public Vector2 velocity;
    private Vector2 goalPos = Vector2.zero;
    private Vector2 currentForce;

    private AllUnits allUnits;
    private Rigidbody2D rgdBody;

    private void Start()
    {
        rgdBody = GetComponent<Rigidbody2D>();
        allUnits = manager.GetComponent<AllUnits>();
        velocity = new Vector2(Random.Range(0.01f, 0.1f), Random.Range(0.01f, 0.1f));
        location = new Vector2(transform.position.x, transform.position.y);
    }

    private void Update()
    {
        Flock();
        goalPos = manager.transform.position;
    }

    private Vector2 Seek(Vector2 target)
    {
        return (target - location);
    }

    private void ApplyForce(Vector2 f)
    {
        Vector3 force = new Vector3(f.x, f.y, 0);
        if (force.magnitude > allUnits.maxForce)
        {
            force = force.normalized;
            force *= allUnits.maxForce;
        }
        rgdBody.AddForce(force);
        if (rgdBody.velocity.magnitude > allUnits.maxVelocity)
        {
            rgdBody.velocity = rgdBody.velocity.normalized;
            rgdBody.velocity *= allUnits.maxVelocity;
        }
        Debug.DrawRay(transform.position, force, Color.white);
    }

    private Vector2 Align()
    {
        float neighbourDist = allUnits.neighbourDistance;
        Vector2 avgHeading = Vector2.zero;
        int count = 0;
        foreach (GameObject other in allUnits.units)
        {
            if (other == this.gameObject) continue;
            float d = Vector2.Distance(location, other.GetComponent<Unit>().location);
            if (d < neighbourDist)
            {
                avgHeading += other.GetComponent<Unit>().velocity;
                count++;
            }
        }
        if (count > 0)
        {
            avgHeading /= count;
            Vector2 steer = avgHeading - velocity;
            return steer;
        }
        return Vector2.zero;
    }

    private Vector2 Cohesion()
    {
        float neighbourDist = allUnits.neighbourDistance;
        Vector2 avgHeading = Vector2.zero;
        int count = 0;
        foreach (GameObject other in allUnits.units)
        {
            if (other == this.gameObject) continue;
            float d = Vector2.Distance(location, other.GetComponent<Unit>().location);
            if (d < neighbourDist)
            {
                avgHeading += other.GetComponent<Unit>().location;
                count++;
            }
        }
        if (count > 0)
        {
            avgHeading /= count;

            return Seek(avgHeading);
        }
        return Vector2.zero;
    }

    //private void Flock()
    //{
    //    location = transform.position;
    //    velocity = GetComponent<Rigidbody2D>().velocity;

    //    Vector2 gl;
    //    gl = Seek(goalPos);
    //    currentForce = gl;
    //    currentForce = currentForce.normalized;

    //    ApplyForce(currentForce);
    //}
    //private void Flock()
    //{
    //    location = transform.position;
    //    velocity = rgdBody.velocity;

    //    if (Random.Range(0, 50) <= 1)
    //    {
    //        Vector2 align = Align();
    //        Vector2 coh = Cohesion();
    //        Vector2 gl;
    //        gl = Seek(goalPos);
    //        currentForce = align + coh + gl;

    //        currentForce = currentForce.normalized;
    //    }

    //    ApplyForce(currentForce);
    //}

    private void Flock()
    {
        location = transform.position;
        velocity = rgdBody.velocity;

        if (allUnits.obedient && Random.Range(0, 50) <= 1)
        {
            Vector2 align = Align();
            Vector2 coh = Cohesion();
            Vector2 gl;
            if (allUnits.seekGoal)
            {
                gl = Seek(goalPos);
                currentForce = align + coh + gl;
            }
            else currentForce = align + coh;

            currentForce = currentForce.normalized;
        }
        if (allUnits.willful && Random.Range(0, 50) <= 1)
        {
            if (Random.Range(0, 50) <= 1)//change direction
                currentForce = new Vector2(Random.Range(0.01f, 0.1f), Random.Range(0.01f, 0.1f));
        }
        ApplyForce(currentForce);
    }
}