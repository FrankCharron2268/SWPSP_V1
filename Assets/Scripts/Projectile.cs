﻿using UnityEngine;

public class Projectile : MonoBehaviour
{
    private Rigidbody rigdbody;

    public float speed;                     //Traveling speed of bolts
    public float damage;                   //The dmg they do
    private BaseController controller;    //Reference to hazard player and enemies child controllers.

    private void Start()
    {
        rigdbody = GetComponent<Rigidbody>();
        rigdbody.velocity = transform.forward * speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && gameObject.CompareTag("Projectile Enemy") || other.CompareTag("Enemy") && gameObject.CompareTag("Projectile Player"))
        {
            if (other.CompareTag("Player"))
            {
                controller = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
                controller.CalculateDamage(damage);
            }
            else if (other.CompareTag("Enemy") || other.CompareTag("Hazard"))
            {
                controller = other.GetComponent<BaseController>();
                controller.CalculateDamage(damage);
            }
            Destroy(gameObject);
        }
    }
}